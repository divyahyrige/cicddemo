package com.example.controller;

import com.example.service.avgService;
//import org.renjin.script.RenjinScriptEngineFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.renjin.script.*;
import javax.script.*;

//import org.rosuda.JRI.Rengine;
//import org.rosuda.JRI.REXP;


@RestController
@ComponentScan(basePackageClasses = avgService.class)
public class AvgController {

    //    @Autowired
    private avgService avgservice;
    /**
     * Autowiring service obeject in cotnroller
     *
     */
    public AvgController(avgService avgService) {
        this.avgservice = avgService;
    }

    @RequestMapping(value = "/Hello")
    public String getHello() {
        System.out.println("start controller here");
        return "Hello from gitlab to EC2 from the team!";
    }


    /**
     * REST API to read csv file and process the data.
     *
     * using service reference upload method of service class is called and
     * csv file is passed to upload method
     */
    @PostMapping("/upload")
    public double createConsumer(@RequestParam("file") MultipartFile file) throws Exception {
        System.out.println("in trial");
        return avgservice.upload(file);
    }


}
