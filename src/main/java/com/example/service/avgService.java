package com.example.service;

import com.example.controller.RUtils;
import com.github.rcaller.rstuff.RCaller;
import com.github.rcaller.rstuff.RCallerOptions;
import com.github.rcaller.rstuff.RCode;
import org.renjin.eval.Session;
import org.renjin.eval.SessionBuilder;
//import org.renjin.script.RenjinScriptEngineFactory;
import org.renjin.sexp.DoubleArrayVector;
import org.renjin.sexp.SEXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

//import javax.script.ScriptEngine;
//import javax.script.ScriptException;
import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import org.renjin.script.*;
import javax.script.*;

@Service
public class avgService {


    /**
     * Invokes the customMean R function passing the given values as arguments.
     * using Renjin call R program
     * @param values the input to the mean script
     * @return the result of the R script
     * @throws IOException        if any error occurs
     * @throws URISyntaxException if any error occurs
     * @throws ScriptException    if any error occurs
     */
    public double mean(int[] values) throws IOException, URISyntaxException, ScriptException, URISyntaxException {
//        RenjinScriptEngine engine = new RenjinScriptEngine();
        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
        // create a Renjin engine:
        ScriptEngine engine = factory.getScriptEngine();
        String meanScriptContent = RUtils.getMeanScriptContent();
        engine.put("input", values);
        engine.eval(meanScriptContent);
        DoubleArrayVector result = (DoubleArrayVector) engine.eval("customMean(input)");
        return result.asReal();
    }
    /**
     * Connects to the Rserve istance and invokes the
     * customMean R function passing the given values as arguments.
     *
     * using Rserve to call R script
     * @param values the input to the mean script
     * @return the result of the R script
     * @throws REngineException      if any error occurs
     * @throws REXPMismatchException if any error occurs
     */
    public double mean1(int[] values) throws REngineException, REXPMismatchException, REngineException, REXPMismatchException {
        RConnection c = new RConnection();
        c.assign("input", values);
        return c.eval("customMean(input)")
                .asDouble();
    }


    /**
     * Invokes the customMean R function passing the given values as arguments.
     *
     * using Rcaller to call R program
     * @param values the input to the mean script
     * @return the result of the R script
     * @throws IOException        if any error occurs
     * @throws URISyntaxException if any error occurs
     */
    public double mean2(int[] values) throws IOException, URISyntaxException {
        String fileContent = RUtils.getMeanScriptContent();
        RCode code = RCode.create();
        code.addRCode(fileContent);
        code.addIntArray("input", values);
        code.addRCode("result <- customMean(input)");
        RCaller caller = RCaller.create(code, RCallerOptions.create());
        caller.runAndReturnResult("result");
        return caller.getParser()
                .getAsDoubleArray("result")[0];
    }

    /**
     * Invokes the customMean R function passing the given values as arguments.
     *
     * using Renjin call R program using session instance
     * @throws FileNotFoundException if any error occurs
     * @throws ScriptException    if any error occurs
     * @return
     */
    public double avgmean(float[] listofval) throws ScriptException, IOException, REXPMismatchException {
        Session session = new SessionBuilder()
                .withDefaultPackages()
                .build();


        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
//        // create a Renjin engine:
        ScriptEngine engine = factory.getScriptEngine(session);
        engine.put("input",listofval);
        SEXP res = (SEXP) engine.eval(new FileReader("avg.R"));
        System.out.println("result is "+ res);
        double result = Math.round((res.asReal()) * 100) / 100.0d;
        return  result;


    }


    public double upload(MultipartFile file) throws  Exception{
        System.out.println("In service class");
        StringBuilder sb = new StringBuilder();
        ArrayList<String> aa = new ArrayList<>();
        InputStream inputStream = file.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        float[] listofval = new float[10];
        Float[] floats = new Float[10];
        int i = 0;
        while ((line = br.readLine()) != null) {
            floats = Arrays.stream(line.split(",")).map(Float::valueOf).toArray(Float[]::new);
            float a = Math.round((floats[1] - floats[0]) * 100) / 100.0f;
            listofval[i]=a;
            i++;
        }
        for(float l: listofval) {
            System.out.println(l);
        }
        return avgmean(listofval);
    }
}
