package com.example.demo;

import com.example.controller.AvgController;
import org.renjin.script.RenjinScriptEngineFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.FileNotFoundException;


@SpringBootApplication
@ComponentScan(basePackageClasses = AvgController.class)
public class DemoApplication {

	public static void main(String[] args) throws FileNotFoundException, ScriptException {
		System.out.println("start");
		SpringApplication.run(DemoApplication.class, args);
		System.out.println("end");
	}

}
